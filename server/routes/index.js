const {BaseController} = require('../components/base_controller');
let jwt = require('jsonwebtoken');
let config =  require('../config/config');
const {Middleware} = require('../utils');
class Index extends BaseController{

    methods = {
        'actionLogin': {
            action: 'post',
            visitorsOnly: true,
        },
        'actionAuthenticate': {
            action: 'post',
            visitorsOnly: true,
        },
    };
    async actionLogin(req){
        const body = req.body.data;
        const result = await this.app.db.query("select id,name,role from user where email=? and password=?", [body.email, body.password]);
        if (result.length > 0) {
            const token = jwt.sign(
                                { email: body.email }
                                ,config.secret,
                                {
                                    expiresIn: '24h'
                                }
            );
            return { success: true, token, user: result[0] };
        } else {
            return {success : false, msg:  'Username or password does not match.'};
        }
    }
    async actionAuthenticate(req){
        const token = req.body.data;
        if ( !token )
            return {success : false, msg:  'User is not authenticated.'};
        const userData = await new Middleware().checkToken(token);
        if (userData) {
            const result = await this.app.db.query("select id,name,role from user where email=?", [userData.email]);
            if (result.length > 0) {
                return { success: true, token, user: result[0] };
            }
            return { success: false, msg:  'User not found.'};
        } else {
            return {success : false, msg:  'User is not authenticated.'};
        }
    }
}
exports.Index = Index;