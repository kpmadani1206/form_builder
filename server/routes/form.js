const {BaseController} = require('../components/base_controller');
const {getKey} = require("../utils");
const moment = require('moment');
class Form extends BaseController{

    methods = {
        actionGetForms: {
            action: 'get',
            visitorsOnly: true,
        },
        actionPreview: {
            action: 'get',
            visitorsOnly: true,
        },
        actionCreateForm: {
            action: 'post',
            visitorsOnly: true,
        },
        actionSubmitForm: {
            action: 'post',
            visitorsOnly: true,
        },
    };

    async actionGetForms(req){
        const {page = 0, limit = 5} = req.query;
        const skip = ((page-1)*limit);
        const totalCountQuery = "select count(id) as count from forms";
        const getFormsQuery = "select * from forms";
        try {
            const result = await this.app.db.query(getFormsQuery, [limit, skip]);
            const totalCountResult = await this.app.db.query(totalCountQuery);
            return { success: true, forms: result, totalCount: totalCountResult[0] ? totalCountResult[0].count : 1};
        } catch (e) {
            return { success: false };
        }
    }
    async actionPreview(req){
        const {slug} = req.query;
        const getFormSql = "select * from forms where short_url = ?";
        try {
            const result = await this.app.db.query(getFormSql, [slug]);
            return {success: true, result};
        } catch (e) {
            console.log('Error while previewing form:', e);
            return { success: false };
        }
    }
    async actionCreateForm(req){
        const {name, config} = req.body.data;
        const shortUrl = getKey(name);
        const sql = "insert into forms (name,config,short_url,responses, date_created) values (?,?,?,?,?)";
        console.log('date:', moment().format('YYYY-MM-DD hh:mm:ss'));
        try {
            await this.app.db.query(sql, [name, JSON.stringify(config), shortUrl , 0, moment().format('YYYY-MM-DD hh:mm:ss')]);
            const form = {
                name,
                config,
                short_url: shortUrl,
                responses: 0,
            };
            return {success: true, form};
        } catch (e) {
            console.log('Error while creating form:', e);
            return {success: false};
        }
    }
    async actionSubmitForm(req){
        const {id} = req.body.data;
        const getResponseSql = 'select responses from forms where id = ?';
        const result = await this.app.db.query(getResponseSql, [id]);
        if (Array.isArray(result) && result.length > 0) {
            const responses = result[0].responses;
            const sql = "update forms set responses = ? where id = ?";
            await this.app.db.query(sql, [(responses+1), id]);
            return { success: true };
        } else {
            return { success: false };
        }
    }
}
exports.Form = Form;