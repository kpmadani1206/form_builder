const {BaseModule} = require('./components/base_module.js');
class Master {
    async start(){
        const module = new BaseModule();
        await module.init();
    }
}
new Master().start();