const express =  require('express');
const fs = require("fs");
const path = require('path');
const multer = require("multer");

class BaseModule{

    webServer;
    httpServer;
    db;
    routes = {};
    env = 'dev';

    constructor() {
        (global).app = this;
    }

    async init() {
        await this.startWebService(this.env);
    }

    get webPort() {
        return this.env === 'dev'?3000:80;
    }

    async startWebService(env) {
        this.env = env;
        if (this.webPort) {
            const release = this.setupWebServer(this.webPort);
            await this.setupComponents(env);
            await this.setupRoutes();
            release();
        } else {
            throw new Error('port is not defined.');
        }
    }

    static app() {
        return (global).app;
    }

    setupWebServer(port) {

        const START_TIME = Date.now();
        this.webServer = express();
        const storage = multer.diskStorage({
            destination: function (req, file, cb) {
                cb(null, __dirname + '../../../client/public/assests/images')
            },
            filename: function (req, file, cb) {
                cb(null, file.originalname);
            }
        });
        const upload = multer({ storage: storage });
        this.webServer.use(express.json());
        this.webServer.use(function (req, res, next) {

            // Website you wish to allow to connect
            res.setHeader('Access-Control-Allow-Origin', '*');

            // Request methods you wish to allow
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

            // Request headers you wish to allow
            res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');

            // Pass to next layer of middleware
            next();
        });
        this.webServer.use(upload.array('avatar',10));
        return (() => {
            this.webServer.listen(port, () => console.log('done now check:',{ port, took: ((Date.now() - START_TIME) / 1000) }, 'web server ready'));
        });
    }
    readDirFiles(dir) {
        const list = [];
        for (const file of fs.readdirSync(dir)) {
            const fullPath = path.join(dir, file);
            if (fs.lstatSync(fullPath).isDirectory()) {
                const files = this.readDirFiles(fullPath);
                for (const f of files) {
                    list.push(f);
                }
            } else {
                list.push(fullPath);
            }
        }
        return list;
    }
    async setupRoutes() {
        const routesPath = path.join(process.cwd(), `routes`);
        if (fs.existsSync(routesPath)) {
            const paths = this.readDirFiles(routesPath);
            for (const module of paths) {
                if (module.indexOf('js') > 0) {
                    const name = module.replace(routesPath, '').replace('/', '').replace('.js', '');
                    const uname = name.substring(0,1).toUpperCase() + name.substring(1, name.length);
                    const ccc = require(module);
                    const con = new ccc[uname](this, name);
                    await con.init(con.methods);
                    this.routes[con.getBaseRoute()] = con;
                }
            }
        }
    }
    async setupComponents(env) {

        for (const component of fs.readdirSync(__dirname + '/')) {
            if (
                component.indexOf('.js') > 0 &&
                component.indexOf('base_') !== 0
            ) {
                const fname = component.replace('/', '').replace('.js', '');
                const uname = fname.substring(0,1).toUpperCase() + fname.substring(1, fname.length);
                const con = require('./' + component);
                const base=new (con)[uname]();
                let name = base.getName();
                await base.init(env);
                this[name] = base;
            }
        }
    }

    get apiVersion() {
        return 1;
    }
}
module.exports = {BaseModule};