const mysql = require("mysql");
let config =  require('../config/config');


const RETRY_QUERY_ERRORS = [
    "ER_TRANSACTION_ROLLBACK_DURING_COMMIT",
    "ER_LOCK_DEADLOCK",
    "Error: UNKNOWN_CODE_PLEASE_REPORT: Max connect timeout reached while reaching hostgroup 0 after",
];
const MAX_RETRY = 5;
const LOG_ALL_QUERIES = false;

class Database {
    pool;
    pools=[];
    init(environment) {
        this.pool = {
            query: (...args) => this.query.apply(this, args),
            escape: (...args) => mysql.escape.apply(mysql, args),
        };
        let db = config["db_"+environment];
        for (const k in db) {
            const config = db[k];
            if (config == null) {
                continue;
            }
            this.createPool('master',db[0].master, err => {
                if (err) {
                    console.error("initial db connection failed to " + k + " error:" + err.toString());
                } else {
                    console.info("db connected : " + k);
                }
            }, false);
        }
    }

    optimize(pool) {
        pool.acquireConnection = function acquireConnection(connection, cb) {
            if (connection._pool !== this) {
                throw new Error("Connection acquired from wrong pool.");
            }

            if (this._closed) {
                const err = new Error("Pool is closed.");
                this._connectionQueue.unshift(cb);
                this._purgeConnection(connection);
                return;
            }

            this.emit("acquire", connection);
            cb(null, connection);
        };
        return pool;
    }

    createPool(zone, config, cb, connect_required= false) {
        this.pools[zone] = this.pools[zone] ? this.pools[zone] : this.optimize(mysql.createPool({
            connectionLimit: typeof config.connectionLimit !== "undefined" ? config.connectionLimit : 8,
            host: config.host,
            user: config.user,
            port: config.port || "3306",
            database: config.db,
            dateStrings: true,
            password: config.password,
            multipleStatements: false,
        }));

        this.pools[zone].getConnection((err, connection) => {
            if (connection) {
                connection.release();
            }
            if (connect_required && err) {
                return setTimeout(() => {
                    console.error("db connection failed to " + zone + ", required, retrying");
                    this.createPool(zone, config, cb, connect_required);
                }, 2000);
            } else if (err) {
                // keep retrying..
                setTimeout(() => {
                    console.error("db connection failed to " + zone + ", optional, retrying");
                    this.createPool(zone, config, null, false);
                }, 2000);
            }
            cb ? cb(err) : null;
        });
    }

    async query( query, param= null, callback= null, retry= 0) {
        const pool = this.pools['master'];
        if (!pool) {
            if (typeof callback == "function") {
                callback('no zone found.');
            }
            return;
        }
        return new Promise((resolve, reject) => {
            pool.query(query, param, (err, data) => {
                if (err) {
                    err = err.toString();
                    for (const e of RETRY_QUERY_ERRORS) {
                        if (err.indexOf(e) >= 0) {
                            if (retry < MAX_RETRY) {
                                return this.query(query, param, callback, retry + 1);
                            }
                        }
                    }
                    reject(err);
                }
                resolve(data);
            });
        });
    }

    configure_pool(pool, key, cb) {
        pool.getConnection((err, connection) => {
            // Use the connection
            if (err) {
                console.error("Error when connected db:", err);
                setTimeout(() => {
                    this.configure_pool(pool, key, cb);
                }, 2000);
            } else {
                // Why It is called twice each time server starts ? Is there 2 worker or something in local environment?
                console.info(`connected: ${key}`);
                cb();
            }
        });
    }
    getName() {
        return "db";
    }
}
module.exports = {Database};