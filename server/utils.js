let jwt = require('jsonwebtoken');
let config =  require('./config/config');
const getKey = text => {
    return text.toString().toLowerCase().trim()
        .replace(/&/g, '-and-')         // Replace & with 'and'
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[&\/\\#,+()$~%.`'":*?<>{}^@!_=-[\]|;]/g, '')       // Remove all special chars
        .replace(/--+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
};
class Middleware{
    async checkToken(token){
        return new Promise((resolve, reject) => {
            if (token && token.startsWith('Bearer ')) {
                token = token.slice(7, token.length);
            }
            if (token) {
                jwt.verify(token, config.secret, (err, data) => {
                    if (err) {
                        resolve(false);
                    } else {
                        resolve(data);
                    }
                });
            } else {
                resolve(false);
            }
        })
    }
}
module.exports = {Middleware, getKey};