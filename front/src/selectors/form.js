export function getFormsList( state ) {
    return state.forms.forms || [];
}
export function checkForLive( state ) {
    return state.forms.isLive || false;
}
export function getActiveForm( state ) {
    return state.forms.activeForm;
}
export function getIsLoading( state ) {
    return state.forms.loading;
}