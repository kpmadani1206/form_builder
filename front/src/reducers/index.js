import { combineReducers as combine } from 'redux';
import forms from './forms';
export default combine({
    forms,
});