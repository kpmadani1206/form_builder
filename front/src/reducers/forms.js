const reducer = (state = {
    forms: [],
    isLive: false,
    activeForm: {
        name: '',
        config:[],
    },
    loading: false,
}, action) => {
    switch (action.type) {
        case 'GET_FORMS':
            return { ...state, loading: true, isLive: false };
        case 'FORMS_RECEIVED':
            return { ...state, forms: action.forms.forms || [], loading: false };
        case 'UPDATE_ACTIVE_FORM':
            return {...state, activeForm: action.formDetails};
        case 'CREATE_FORM':
            return {...state, loading: true};
        case 'CREATE_FORM_RECEIVED':
            const activeForm = {
                name: '',
                config:[{
                    name: '',
                    type: 'text',
                    value: '',
                }],
            };
            if ( !action.form.success ) return state;
            return {...state, loading: false, forms: [...state.forms, action.form.form], activeForm};
        case 'PREVIEW_FORM_RECEIVED':
            if ( !action.form.result[0] ) return state;
            const form = action.form.result[0];
            form.config = JSON.parse(form.config);
            return {...state, loading: false, isLive:true, activeForm: form};
        case "SUBMIT_PREVIEW_FORM":
            return {...state, loading: false, isLive:true, activeForm: {
                    name: '',
                    config:[],
                }};
        default:
            return state;
    }
};
export default reducer;