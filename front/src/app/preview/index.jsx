import React from 'react';
import {checkForLive, getActiveForm} from "../../selectors/form";
import {updateActiveForm} from "../../actions/forms";
import {connect} from "react-redux";
import {useParams} from "react-router-dom";
const Preview = (props) => {
    const { formId } = useParams();
    console.log({formId});
    return <div>Preview</div>
}
const mapStateToProps = state => {
    const isLive = checkForLive(state);
    const formDetails = getActiveForm(state);
    return {isLive, formDetails};
}
export default connect(mapStateToProps, {updateActiveForm})(Preview)