import React from 'react';
import Text from "./text.jsx";
import MultiChoiceInput from "./multi-choice-input.jsx";
import {checkForLive, getActiveForm} from "../../selectors/form";
import {submitPreviewForm, updateActiveForm} from "../../actions/forms";
import {connect} from "react-redux";
import FieldSelection from "./field-selection.jsx";
import { useHistory } from "react-router-dom";
const Container = (props) => {
    let {isLive, formDetails, preview} = props;
    const history = useHistory();
    if ( preview ) isLive = true;
    const renderConfig = (config, index) => {
        if ( config.type === 'text' ) {
            return <Text config={config} isLive={isLive} onParentChange={(key, value) => {
                const copy = {...formDetails};
                copy.config[index][key] = value;
                props.updateActiveForm(copy);
            }}
            updateValue={(value) => {
                const copy = {...formDetails};
                copy.config[index].value = value;
                props.updateActiveForm(copy);
            }}
            />
        }
        else if ( config.type === 'checkbox' || config.type === 'radio') {
            return <MultiChoiceInput config={config} isLive={isLive} addOptions={(key, value) => {
                const copy = {...formDetails};
                copy.config[index].options.push({
                    [key]: value,
                });
                props.updateActiveForm(copy);
            }} updateName={(name) => {
                const copy = {...formDetails};
                copy.config[index].name = name;
                props.updateActiveForm(copy);
            }}
            updateValue={(selectedIndex, shouldAdd) => {
                const copy = {...formDetails};
                if (!copy.config[index].value) copy.config[index].value =[];
                if ( shouldAdd ) copy.config[index].value.push(selectedIndex);
                else copy.config[index].value.splice(selectedIndex,1);
                props.updateActiveForm(copy);
            }}
            />
        }
    }
    const onDelete = (index) => {
        const copy = {...formDetails};
        copy.config.splice(index,1);
        props.updateActiveForm(copy);
    }
    return <div className={'component-container' + (isLive ? ' preview ' : '')}>
        {
            formDetails.config.length === 0 ?
                "No Components Added in form" : ''
        }
        {
            isLive && !preview && <h1>{formDetails.name}</h1>
        }
        <React.Fragment>
            {
                formDetails.config.map((config, index) => {
                    return <div className={'config-row'} key={index}>
                        {!isLive && <FieldSelection index={index}/>}
                        {renderConfig(config, index)}
                        {!isLive && <button className={'delete-btn'} onClick={() => {
                            onDelete(index);
                        }}>Remove</button>}
                    </div>
                })
            }
        </React.Fragment>
        {
            isLive && !preview && <button onClick={()=> {
                props.submitPreviewForm(formDetails);
                history.push('/');
            }}>Submit</button>
        }
    </div>
}
const mapStateToProps = state => {
    const isLive = checkForLive(state);
    const formDetails = getActiveForm(state);
    return {isLive, formDetails};
}
export default connect(mapStateToProps, {updateActiveForm, submitPreviewForm})(Container)