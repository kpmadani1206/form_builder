import React from 'react';
import {connect} from "react-redux";
import {updateActiveForm} from "../../actions/forms";
const Text = (props) => {
    const {isLive = false, config} = props;
    return <React.Fragment>
        {
            isLive ?
                <React.Fragment>
                    <label>{config.name}</label>
                    <input disabled={!isLive} placeholder={isLive?'Enter Value' : 'This is demo of text box how it looks in preview'}
                           onChange={(e) => {
                               props.updateValue(e.target.value);
                           }}
                    />
                </React.Fragment>:
                <input value={config.name} placeholder={'Enter Label Value For Text Field'} onChange={(e) => {
                    props.onParentChange('name', e.target.value);
                }}/>
        }
    </React.Fragment>
}
export default connect(null, {updateActiveForm})(Text);