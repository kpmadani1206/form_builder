import React from 'react';
import {checkForLive, getActiveForm} from "../../selectors/form";
import {updateActiveForm} from "../../actions/forms";
import {connect} from "react-redux";
const FieldSelection = (props) => {
    let {formDetails, index} = props;
    const onFieldChange = (type) => {
        const copy = {...formDetails};
        copy.config[index].type = type;
        copy.config[index].options = [];
        props.updateActiveForm(copy);
    }
    return <select onChange={(e) => {
        onFieldChange(e.target.value);
    }}>
        <option value={'text'}>Text Field</option>
        <option value={'checkbox'}>Multi choice Checkbox</option>
        <option value={'radio'}>Single Select Radio</option>
    </select>
}
const mapStateToProps = state => {
    const isLive = checkForLive(state);
    const formDetails = getActiveForm(state);
    return {isLive, formDetails};
}
export default connect(mapStateToProps, {updateActiveForm})(FieldSelection)