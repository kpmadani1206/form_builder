import React, {useState} from 'react';
import {getKey} from "../utils/utils";
const MultiChoiceInput = (props) => {
    const {addOptions, isLive, config} = props;
    const {name, type, options, short_url} = config;
    const [newLabel, setNewLabel] = useState('');
    const renderLiveElements = () => {
        return <React.Fragment>
            <label>{name || 'Please Enter label for ' + type}</label>
            {
                options.map((option, index) => {
                    return <div key={index} className={'items'}>
                        <label>{option.label}</label>
                        <input value={option.label} type={type} name={short_url || getKey(name || '')} onChange={(e) => {
                            props.updateValue(index, e.target.checked);
                        }}/>
                    </div>
                })
            }
        </React.Fragment>
    }
    const renderEditElements = () => {
        return <React.Fragment>
            <input placeholder={'Please Enter label for '+ type} onChange={(e) => props.updateName(e.target.value)} value={name}/>
            <div className={'form-options'}>
                <input placeholder={'Please Enter label for '+ type +' option'} onChange={(e) => setNewLabel(e.target.value)} value={newLabel}/>
                <button className={'items add-more-btn'}  onClick={() => {
                    if ( !newLabel ) {
                        alert('Please Enter Option Label');
                        return;
                    }
                    addOptions('label',newLabel);
                    setNewLabel('');
                }}>Add {options.length > 0 ? 'more' : ''} Option</button>
            </div>
        </React.Fragment>
    }
    return <div className={'component-container'}>
        <div>
            {
                isLive?
                    renderLiveElements()
                    :
                    renderEditElements()
            }
        </div>
    </div>
}
export default MultiChoiceInput;