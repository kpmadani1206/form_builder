import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {checkForLive, getActiveForm, getIsLoading} from '../../selectors/form';
import AddUpdateDialog from '../add-update-dialog/index.jsx';
import {submitForm} from "../../actions/forms";
import './style.css';
const Header = (props) => {
    const {formDetails, isLoading, isLive} = props;
    const [isOpen, setIsOpen]  = useState(false);
    useEffect(() => {
        if ( isOpen && !isLoading) setIsOpen(false);
    },[isLoading]);
    const toggleFormDialog = () => {
        setIsOpen(!isOpen);
    }
    const submitForm = () => {
        if ( !formDetails.name ) {
            alert('Please Enter Form Name');
            return;
        }
        if ( formDetails.config.length === 0 ) {
            alert('Please select at least one component for form.');
            return;
        }
        let errors = '';
        formDetails.config.forEach((config, index) => {
            if ( config.type === 'text' && !config.name) {
                errors += '\nPlease enter name for text field.'
            }
            if ( config.type === 'radio' || config.type === 'checkbox') {
                if (!config.name)
                    errors += '\nPlease enter name for field.'
                if ( config.options.length === 0 )
                    errors += '\nPlease enter name of option and click Add Option.'
                config.options.forEach(option => {
                    if ( !option.label ) {
                        errors += '\nPlease enter label for field.'
                    }
                });
            }
        });
        if ( errors ){
            alert(errors);
            return;
        }
        props.submitForm(formDetails);
    }
    return isLive ? '' :
        <React.Fragment>
            <div className={'header'}>
                <h1>Simform Form Builder</h1>
                <button onClick={toggleFormDialog}>Add Form</button>
            </div>
            <AddUpdateDialog isOpen={isOpen} toggleFormDialog={toggleFormDialog} submitForm={submitForm}/>
        </React.Fragment>
}
const mapStateToProps = state => {
    const formDetails = getActiveForm(state);
    const isLoading = getIsLoading(state);
    const isLive = checkForLive(state);
    return {formDetails, isLoading, isLive};
}
export default connect(mapStateToProps, {submitForm})(Header)