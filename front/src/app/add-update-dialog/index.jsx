import React, {useEffect, useState} from 'react';
import './style.css';
import Modal from "react-modal";
import Container from "../components/container.jsx";
import {connect} from "react-redux";
import {updateActiveForm} from "../../actions/forms";
import {checkForLive, getActiveForm} from "../../selectors/form";
Modal.setAppElement("#root");
const FormDialog = (props) => {
    const {isOpen, toggleFormDialog, formDetails, submitForm} = props;
    return <div>
        <Modal
            isOpen={isOpen}
            onRequestClose={toggleFormDialog}
            contentLabel="My dialog"
            className="add-update-modal"
            overlayClassName="add-update-overlay"
        >
            <div className={'modal-container'}>
                <h3>Add Form Dialog.</h3>
                <React.Fragment>
                    <div className={'title-container'}>
                        <label htmlFor={'name'}>Form Name    </label>
                        <input value={formDetails.name} name={'name'} placeholder={'Enter Form Name'} onChange={(e) =>{
                            const copy = {...formDetails};
                            copy.name = e.target.value;
                            props.updateActiveForm(copy);
                        }}/>
                    </div>
                    <div className={'playground-container'}>
                        <div className={'playground'}>
                            <h3>Playground</h3>
                            <Container/>
                            <button className={'add-component-btn'} onClick={() => {
                                const copy = {...formDetails};
                                copy.config.push({
                                    type: 'text',
                                    value: '',
                                    name: '',
                                });
                                props.updateActiveForm(copy);
                            }}>Add {formDetails.config.length === 0 ? ' First ' : ' More '} Component</button>
                        </div>
                        <div className={'preview'}>
                            <h3>Preview of Form</h3>
                            <Container preview={true}/>
                        </div>
                    </div>
                </React.Fragment>
                <button onClick={submitForm}>Save Form</button>
            </div>
        </Modal>
    </div>
}
const mapStateToProps = state => {
    const isLive = checkForLive(state);
    const formDetails = getActiveForm(state);
    return {isLive, formDetails};
}
export default connect(mapStateToProps, {updateActiveForm})(FormDialog)