import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {getActiveForm, getFormsList, getIsLoading} from '../../selectors/form';
import './style.css';
import {getForms, getPreviewForm} from "../../actions/forms";
import Container from '../components/container.jsx';
import {useHistory} from "react-router-dom";
import moment from 'moment';
const Index = (props) => {
    const [slug, setSlug] = useState('');
    const history = useHistory();
    useEffect(() => {
        return history.listen((location) => {
            checkSlug();
        })
    },[history])
    useEffect(() => {
        checkSlug();
    }, []);
    const checkSlug = () => {
        const params = new URLSearchParams(window.location.search);
        let slug = params.get('slug');
        if ( !slug ) {
            setSlug('');
            props.getForms();
        }else {
            setSlug(slug);
            props.getPreviewForm(slug);
        }
    }
    const {forms} = props;
    const renderList = () => {
        return <div className={'list-container'}>
            <h1>Forms List</h1>
            {
                forms.length === 0 ?
                    "No Forms Created."
                    :
                    <div className={'list-container'}>
                        <table>
                            <thead>
                            <tr>
                                <td>Name</td>
                                <td>URL To Preview Form</td>
                                <td>Created Date</td>
                                <td>Number of Responses</td>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                forms.map((form, index) => {
                                    return <tr key={index}>
                                        <td>
                                            {form.name}
                                        </td>
                                        <td>
                                            <a href={'?slug='+form.short_url} target={'_blank'}>Preview</a>
                                        </td>
                                        <td>
                                            {form.date_created || 'NA'}
                                        </td>
                                        <td>
                                            {form.responses || 0}
                                        </td>
                                    </tr>
                                })
                            }
                            </tbody>
                        </table>
                    </div>
            }
        </div>
    }
    return slug ? <Container/> : renderList();
}
const mapStateToProps = state => {
    const forms = getFormsList(state);
    const formDetails = getActiveForm(state);
    const isLoading = getIsLoading(state);
    return {forms, formDetails, isLoading};
}
export default connect(mapStateToProps, {getForms, getPreviewForm})(Index)