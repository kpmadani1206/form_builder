export const getKey = text => {
    return text.toString().toLowerCase().trim()
        .replace(/&/g, '-and-')         // Replace & with 'and'
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[&\/\\#,+()$~%.`'":*?<>{}^@!_=-[\]|;]/g, '')       // Remove all special chars
        .replace(/--+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
};