import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import List from './list/index.jsx';
import './style.css';
import Header from "./components/header.jsx";
const App = () => {
    return <Router>
            <div>
                <div className='main-header'>
                    <Header/>
                </div>
                <Switch>
                    <Route path="/">
                        <List/>
                    </Route>
                </Switch>
            </div>
    </Router>
}
export default App;