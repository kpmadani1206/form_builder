import 'regenerator-runtime/runtime'
import { put, takeLatest, all } from 'redux-saga/effects';

const BASE_API_URL = 'http://localhost:3000/api/v1/';

function* fetchForms() {
    const json = yield fetch(BASE_API_URL + 'get-forms')
        .then(response => response.json());
    yield put({ type: "FORMS_RECEIVED", forms: json.data, });
}
function* actionFormsWatcher() {
    yield takeLatest('GET_FORMS', fetchForms)
}
function* createForm(data) {
    const json = yield fetch(BASE_API_URL + 'create-form', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }).then(response => response.json());
    yield put({ type: "CREATE_FORM_RECEIVED", form: json.data });
}
function* actionCreateFormWatcher() {
    yield takeLatest('CREATE_FORM', createForm)
}
function* getPreviewForm(data) {
    const json = yield fetch(BASE_API_URL + 'preview/?slug='+data.slug)
        .then(response => response.json());
    yield put({ type: "PREVIEW_FORM_RECEIVED", form: json.data });
}
function* actionGetPreviewFormWatcher() {
    yield takeLatest('GET_PREVIEW_FORM', getPreviewForm)
}
function* submitForm(data) {
    const json = yield fetch(BASE_API_URL + 'submit-form', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }).then(response => response.json());
}
function* actionSubmitFormWatcher() {
    yield takeLatest('SUBMIT_PREVIEW_FORM', submitForm)
}
export default function* rootSaga() {
    yield all([
        actionFormsWatcher(),
        actionCreateFormWatcher(),
        actionGetPreviewFormWatcher(),
        actionSubmitFormWatcher(),
    ]);
}