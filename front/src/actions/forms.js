export const getForms = () => ({
    type: 'GET_FORMS',
});
export const updateActiveForm = (formDetails) => ({
    type: 'UPDATE_ACTIVE_FORM',
    formDetails,
});
export const submitForm = (data) => ({
    type: 'CREATE_FORM',
    data,
});
export const submitFormCompleted = (data) => ({
    type: 'CREATE_FORM_RECEIVED',
    data,
});
export const getPreviewForm = (slug) => ({
    type: 'GET_PREVIEW_FORM',
    slug,
});
export const submitPreviewForm = (data) => ({
    type: 'SUBMIT_PREVIEW_FORM',
    data,
});